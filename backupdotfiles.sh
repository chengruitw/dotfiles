#!/bin/bash

echo "$BASH_SOURCE"
SCRIPTDIR=$(dirname "$BASH_SOURCE")
echo "$SCRIPTDIR"/
cp -i -v ~/.vimrc "$SCRIPTDIR"
cp -i -v ~/.tmux.conf "$SCRIPTDIR"
