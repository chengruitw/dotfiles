" Vundle
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'scrooloose/nerdtree.git'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'majutsushi/tagbar.git'
Plugin 'valloric/youcompleteme'
Plugin 'chazy/cscope_maps'
" trying
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'

Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
call vundle#end()            " required
filetype plugin indent on    " required
syntax on

set ai
set cursorline
set nu
set expandtab
set tabstop=4
set shiftwidth=4
set hlsearch

set clipboard+=unnamed " Yanks go on clipboard instead.

set updatetime=750
set background=dark
set t_Co=256 "256color
"set cscopetag


"highlight OverLength ctermbg=red ctermfg=white guibg=#592929
"match OverLength /\%81v.\+/

set list
set listchars=tab:\|\ ,trail:·



"inoremap ( ()<Esc>i
"inoremap " ""<Esc>i
"inoremap ' ''<Esc>i
"inoremap [ []<Esc>i
inoremap {<CR> {<CR>}<Esc>ko
""inoremap {{ {}<ESC>i

"ultisnips
let g:UltiSnipsExpandTrigger = '<C-j>'
let g:UltiSnipsJumpForwardTrigger = '<C-j>'
let g:UltiSnipsJumpBackwardTrigger = '<C-k>'

"Tagbar & NERDtree
nmap <F9> :TagbarToggle<CR>
nmap <F8> :NERDTreeToggle<CR>

set exrc
set secure

